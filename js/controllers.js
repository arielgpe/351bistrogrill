/**
 * Created by Ariel Guzman on 3/23/15
 */
angular.module( 'bistroGrill')

  .controller('GlobalController', function($scope, $timeout, $localStorage, $mdDialog){
      $scope.$storage = $localStorage;

      $scope.logOut = function(ev){
          var confirm = $mdDialog.confirm()
            .title('Quieres cerrar sesion?')
            .content('Si cierras sesion ahora, el progreso no salvado se perdera')
            .ok('Hazlo')
            .cancel('Cancelar')
            .targetEvent(ev);
          $mdDialog.show(confirm).then(function(ev) {
              console.log('You decided to get rid of your debt.');
              delete $localStorage.user;
              delete $localStorage.logged;
              delete $localStorage.loggedOut;
              delete $localStorage.selectedMesa;
              delete $localStorage.values;
              $localStorage.loggedOut = true;
          }, function() {
              console.log('You decided to keep your debt.');
          });
      }
  })
  .controller('LoginController', function($scope, $timeout, $localStorage, $mdToast){
      var db = new PouchDB("351Bistro");
      $scope.$storage = $localStorage;
      $scope.user = {
          username: '',
          password: ''
      };
      $scope.login = function(){
          db.get("usuarios").then(function(doc){
              $scope.usuarios = doc.datos;
              doc.datos.forEach(function(user){
                  if (user.username == $scope.user.username && user.password == $scope.user.password){
                      $localStorage.user = user;
                      $localStorage.logged = true;
                      $localStorage.loggedOut = false;
                      $scope.user = {
                          username: '',
                          password: ''
                      };
                  }
              })
          });
      }
  })

  .controller('MesasController', function($scope, $timeout, $localStorage){
      var db = new PouchDB("351Bistro");
      $scope.$storage = $localStorage;
      db.get("mesas").then(function(doc){
          $timeout(function () {
              $scope.mesas = doc.datos;
          });
      });

      $scope.doSomething = function(mesaId) {
          console.log(mesaId);
          $scope.data.selectedIndex = 1;
          db.get("mesas").then(function(doc){
              doc.datos.forEach(function(mesa){
                  if (mesaId == mesa.id){
                      var index = doc.datos.indexOf(mesa);
                      mesa.status = "rojo";
                      doc.datos[index] = mesa;
                      $scope.$storage.selectedMesa =doc.datos[index];
                      console.log(doc.datos[index]);
                      return db.put(doc);
                  }
              })
          });
      }
  })

  .controller("ConfiguracionController", function($scope, $mdDialog, $timeout){
      var db = new PouchDB("351Bistro");

      $scope.items = ['productos', 'usuarios'];
      $scope.selectedMenu = $scope.items[0];

      db.get("categorias").then(function(doc){
          $scope.categorias = doc.datos;
      });

      db.get("productos").then(function(doc){
          $scope.productos = doc.datos;
      });

      db.get("mesas").then(function(doc){
          $scope.mesas = doc.datos;
      });

      db.get("areas").then(function(doc){
          $scope.areas = doc.datos;
      });

      db.get("usuarios").then(function(doc){
          $scope.usuarios = doc.datos;
      });

      $scope.showCreateProduct = function(ev) {
          $mdDialog.show({
              controller: ProductoController,
              templateUrl: 'templates/dialogs/nuevoproducto.tmpl.html',
              targetEvent: ev
          })
            .then(function(answer) {
                //
                db.get("productos").then(function(doc){
                    $scope.productos = doc.datos;
                });
                //$scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.alert = 'You cancelled the dialog.';
            });
      };

      $scope.showCreateCategoria = function(ev) {
          $mdDialog.show({
              controller: CategoriaController,
              templateUrl: 'templates/dialogs/nuevacategoria.tmpl.html',
              targetEvent: ev
          })
            .then(function(answer) {
                //
                db.get("categorias").then(function(doc){
                    $scope.categorias = doc.datos;
                });
                //$scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.alert = 'You cancelled the dialog.';
            });
      };


      $scope.showCreateMesa = function(ev) {
          $mdDialog.show({
              controller: DialogController,
              templateUrl: 'templates/dialogs/nuevamesa.tmpl.html',
              targetEvent: ev
          })
            .then(function(answer) {
                //
                db.get("mesas").then(function(doc){
                    $scope.mesas = doc.datos;
                });
            }, function() {
                $scope.alert = 'You cancelled the dialog.';
            });
      };

      $scope.showCreateArea = function(ev) {
          $mdDialog.show({
              controller: AreaController,
              templateUrl: 'templates/dialogs/nuevaarea.tmpl.html',
              targetEvent: ev
          })
            .then(function(answer) {
                //
                db.get("areas").then(function(doc){
                    $scope.areas = doc.datos;
                });
            }, function() {
                $scope.alert = 'You cancelled the dialog.';
            });
      };


      $scope.showCreateUsuario = function(ev) {
          $mdDialog.show({
              controller: UsuarioController,
              templateUrl: 'templates/dialogs/nuevousuario.tmpl.html',
              targetEvent: ev
          })
            .then(function(answer) {
                //
                $timeout(function(){
                    db.get("usuarios").then(function(doc){
                        $scope.usuarios = doc.datos;
                    });
                });

            }, function() {
                $scope.alert = 'You cancelled the dialog.';
            });
      };
  })
  .controller("ReporteController", function($scope, $localStorage){
      var db = new PouchDB("351Bistro");
      $scope.$storage = $localStorage;

      $scope.inData = {
          startDate: "",
          endDate: "",
          userId: "",
          mesaId: ""
      };

      $scope.generarReporte = function(){
          var startDate = moment.unix(Date.parse($scope.inData.startDate)/1000).format("YYYY-MM-DD") ;
          var endDate = moment.unix(Date.parse($scope.inData.endDate)/1000).format("YYYY-MM-DD") ;
          var range  = moment().range(startDate, endDate);

          $scope.$storage.reporte = [];
          range.by("days", function(mment){
              var momentUnix = moment.unix(Date.parse(mment) / 1000).format("YYYY-MM-DD");
              db.get("facturas").then(function(doc){
                  doc.datos.forEach(function (factura){
                      factura.mesa_id = Number(factura.mesa_id);
                      factura.mesa_id = Number(factura.usuario_id);
                      $scope.inData.userId = Number($scope.inData.userId);
                      $scope.inData.mesaId = Number($scope.inData.mesaId);
                      var createdUnix = moment.unix(Date.parse(factura.created_at) / 1000).format("YYYY-MM-DD");
                      if (createdUnix == momentUnix && factura.usuario_id == $scope.inData.userId && factura.mesa_id
                        == $scope.inData.mesaId){
                          $scope.$storage.reporte.push(factura);
                      }

                  });

              })
          })
      }
  })

  .controller("VentasController", function($scope,$mdDialog, $localStorage, $timeout){
      $scope.$storage = $localStorage;
      if ($scope.$storage.selectedMesa){
          var currentMesa = $scope.$storage.selectedMesa.id;
      }

      calcProductos();
      var db = new PouchDB("351Bistro");
      db.get("productos").then(function(doc){
          var arr = [];
          doc.datos.forEach(function(prod){
              if (prod.categoria == 1){
                  arr.push(prod);
                  console.log(prod.categoria)
              }
          });
          $timeout(function(){
              $scope.productos = arr;
              $scope.prodSel = [];
              $scope.prodSel = JSON.parse(localStorage.getItem(currentMesa));

          });
      });

      db.get("categorias").then(function(doc){
          $scope.categorias = doc.datos;
      });

      $scope.filterCategories = function(catId){
          db.get("productos").then(function(doc){
              var arr = [];
              doc.datos.forEach(function(prod){
                  if (prod.categoria == catId){
                      arr.push(prod);
                      console.log(prod.categoria)
                  }
              });
              $timeout(function(){
                  $scope.productos = arr;
              })
          });
      };

      $scope.remove = function(producto){
          console.log(producto)
      };

      $scope.vender = function(ev){
          $mdDialog.show({
              controller: VenderDialogController,
              templateUrl: 'templates/dialogs/vender.tmpl.html',
              targetEvent: ev
          })
            .then(function(answer) {

                console.log(answer)
                if (answer == "useful"){
                    var mesaId = $scope.$storage.selectedMesa.id;
                    db.get("mesas").then(function(doc){
                        doc.datos.forEach(function(mesa){
                            if (mesaId == mesa.id){
                                localStorage.removeItem(currentMesa);
                                $scope.$storage.values = {
                                    subtotal: 0,
                                    itbis: 0,
                                    total: 0
                                };
                                var index = doc.datos.indexOf(mesa);
                                mesa.status = "verde";
                                doc.datos[index] = mesa;
                                $scope.$storage.selectedMesa =doc.datos[index];
                                console.log(doc.datos[index]);
                                $timeout(function(){
                                    $scope.data.selectedIndex = 0;
                                });
                                delete $scope.$storage.selectedMesa;
                                return db.put(doc);
                            }
                        })
                    });
                }
                //
            }, function() {
                $scope.alert = 'You cancelled the dialog.';
            });
      };

      $scope.limpiarMesa = function(){
          var mesaId = $scope.$storage.selectedMesa.id;
          db.get("mesas").then(function(doc){
              doc.datos.forEach(function(mesa){
                  if (mesaId == mesa.id){
                      localStorage.removeItem(currentMesa);
                      $scope.$storage.values = {
                          subtotal: 0,
                          itbis: 0,
                          total: 0
                      };
                      var index = doc.datos.indexOf(mesa);
                      mesa.status = "verde";
                      doc.datos[index] = mesa;
                      $scope.$storage.selectedMesa =doc.datos[index];
                      console.log(doc.datos[index]);
                      $timeout(function(){
                          $scope.data.selectedIndex = 0;
                      });
                      delete $scope.$storage.selectedMesa;
                      return db.put(doc);
                  }
              })
          });

      };


      $scope.showSelectProducto = function(producto, ev) {
          $mdDialog.show({
              controller: SelectProductController,
              templateUrl: 'templates/dialogs/selectproducto.tmpl.html',
              targetEvent: ev
          })
            .then(function(answer) {
                var currentMesa = $scope.$storage.selectedMesa.id;
                console.log(currentMesa);
                producto.cantidad = answer.cantidad;
                producto.descuento = answer.descuento;
                var arr;
                if (localStorage.getItem(currentMesa)){
                    arr = localStorage.getItem(currentMesa);
                    arr = JSON.parse(arr);
                    arr.push(producto);
                    arr = JSON.stringify(arr);
                    localStorage.setItem(currentMesa, arr);
                } else {
                    arr = [];
                    arr.push(producto);
                    arr = JSON.stringify(arr);
                    localStorage.setItem(currentMesa, arr);
                }
                calcProductos();

            }, function() {
                $scope.alert = 'You cancelled the dialog.';
            });
      };


      $scope.showSelectMesa = function(ev){
          $mdDialog.show({
              controller: SelectMesaDialogCtrl,
              templateUrl: 'templates/dialogs/selectmesa.tmpl.html',
              targetEvent: ev
          })
            .then(function(answer) {

                //
            }, function() {
                $scope.alert = 'You cancelled the dialog.';
            });
      };

      function calcProductos(){
          $scope.$storage.values = {
              subtotal: 0,
              itbis: 0,
              total: 0
          };

          if(localStorage.getItem(currentMesa)) {
              var arry = localStorage.getItem(currentMesa);
              arry = JSON.parse(arry);
              arry.forEach(function (prod) {
                  $scope.$storage.values = {
                      subtotal: $scope.$storage.values.subtotal += +prod.precio * prod.cantidad,
                      itbis: $scope.$storage.values.itbis += +prod.precio * prod.cantidad * 18 / 100,
                      total: $scope.$storage.values.total += +prod.precio * prod.cantidad + (+prod.precio * prod.cantidad * 18 / 100)
                  };
              })
          }
      }

  });

function SelectMesaDialogCtrl($scope, $mdDialog, $mdToast, $localStorage){
    var db = new PouchDB("351Bistro");
    $scope.$storage = $localStorage;
    db.get("mesas").then(function(doc){
        $scope.mesas = doc.datos;
    });

    $scope.doSomething = function(mesaId) {
        console.log(mesaId);
        db.get("mesas").then(function(doc){
            doc.datos.forEach(function(mesa){
                if (mesaId == mesa.id){
                    var index = doc.datos.indexOf(mesa);
                    mesa.status = "rojo";
                    doc.datos[index] = mesa;
                    $scope.$storage.selectedMesa =doc.datos[index];
                    console.log(doc.datos[index]);
                    $mdDialog.hide();
                    return db.put(doc);
                }
            })
        });
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
}

function UsuarioController ($scope, $mdDialog, $mdToast, $localStorage){
    var db = new PouchDB("351Bistro");
    $scope.tiposUsuarios = [
        {
            type: 1,
            name: "Administrador"
        },
        {
            type: 2,
            name: "Usuario"
        }
    ];
    $scope.usuario = {
        username: "",
        password: "",
        nombre: "",
        apellido:"",
        cedula: "",
        tipo: "",
        id: ""
    };

    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {

        if (answer == "cancel"){
            $mdDialog.hide(answer);
        } else {
            if($scope.usuario.nombre == "" || $scope.usuario.cedula =="" || $scope.usuario.password == "" ||
              $scope.usuario.tipo == "" || $scope.usuario.username == ""){
                $mdToast.show($mdToast.simple().content('Debe completar todos los campos!'));
            } else {
                db.get('usuarios').then(function (doc) {
                    var id;
                    var array = doc.datos;
                    if (doc.datos.length != 0) {
                        var length = doc.datos.length - 1;
                        id = doc.datos[length].id + 1;
                    } else {
                        id = 1;
                    }
                    $scope.usuario.id = id;
                    array.push($scope.usuario);
                    doc.datos = array;
                    // put him back
                    console.log(doc.datos);
                    console.log(id);
                    return db.put(doc);
                });
                $mdDialog.hide(answer);
            }
        }
    }

}
function VenderDialogController ($scope, $mdDialog, $mdToast, $localStorage){
    var db = new PouchDB("351Bistro");
    $scope.$storage = $localStorage;

    $scope.dividirQuest = true;
    var currentMesa;
    if ($scope.$storage.selectedMesa){
        currentMesa = $scope.$storage.selectedMesa.id;
    }
    $scope.dividir = {
        cantidad: ''
    }



    $scope.currencies = ["Efectivo", "Tarjeta"];
    $scope.factura = {
        id: '',
        recibido: '',
        descuento: '',
        total: $scope.$storage.values.total.toFixed(2),
        itbis: $scope.$storage.values.itbis.toFixed(2),
        mesa_id: $scope.$storage.selectedMesa.id,
        usuario_id: $scope.$storage.user.id,
        created_at: new Date()
    };

    $scope.$watch("recibido", function(){
        var devuelta = $scope.factura.recibido - $scope.$storage.values.total;
        $scope.factura = {
            devuelta: devuelta.toFixed(2)
        };
    });


    $scope.showDivide = function(){
        $scope.dividirQuest = !$scope.dividirQuest;

    };
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        if (answer == "cancel"){
            $mdDialog.hide(answer);
        } else {
            db.get("facturas").then(function(facturas){
                var id;
                var array = facturas.datos;
                if (facturas.datos.length != 0) {
                    var length = facturas.datos.length - 1;
                    id = facturas.datos[length].id + 1;
                } else {
                    id = 1;
                }
                $scope.factura.id = id;
                array.push($scope.factura);
                facturas.datos = array;
                // put him back
                console.log(id);
                console.log(facturas.datos);
                return db.put(facturas)
            }).then(function(succ){
                db.get("facturas").then(function(facturas) {
                    var length = facturas.datos.length - 1;
                    var facturaId = facturas.datos[length].id;

                    db.get("detalles").then(function(detalles){
                        var id;
                        var array = detalles.datos;
                        if (detalles.datos.length != 0) {
                            var length = detalles.datos.length - 1;
                            id = detalles.datos[length].id +1;
                            console.log(id)
                        } else {
                            id = 1;
                            console.log(id)
                        }
                        var prod = JSON.parse(localStorage.getItem(currentMesa));

                        prod.forEach(function(prod){
                            var obj = {
                                cantidad: prod.cantidad,
                                total: prod.precio,
                                producto_id: prod.id,
                                factura_id: facturaId,
                                descuento: prod.descuento
                            };
                            array.push(obj);
                        });
                        detalles.datos = array;
                        // put him back

                        console.log(detalles.datos);
                        return db.put(detalles);
                    });
                    $mdDialog.hide(answer);
                });
            });
        }
    }

}
function DialogController($scope, $mdDialog, $mdToast) {
    var db = new PouchDB("351Bistro");
    db.get("areas").then(function(doc){
        $scope.areas = doc.datos;
    });
    $scope.mesa  = {
        nombre: '',
        area: '',
        id: '',
        status: "verde"

    };
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        if (answer == "cancel"){
            $mdDialog.hide(answer);
        } else {
            if($scope.mesa.nombre == ""){
                $mdToast.show($mdToast.simple().content('Debe completar todos los campos!'));
            } else {
                db.get('mesas').then(function (doc) {
                    // update his age
                    var id;
                    var array = doc.datos;
                    if(doc.datos.length != 0) {
                        var length = doc.datos.length - 1;
                        id = doc.datos[length].id + 1;
                    } else {
                        id = 1;
                    }
                    $scope.mesa.id = id;
                    array.push($scope.mesa);
                    doc.datos = array;
                    // put him back
                    console.log(doc.datos);
                    console.log(id);
                    return db.put(doc);
                });
                $mdDialog.hide(answer);
            }
        }
    };
}
function CategoriaController($scope, $mdDialog, $mdToast) {
    $scope.categoria  = {
        nombre: '',
        id: ''
    };
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        if (answer == "cancel"){
            $mdDialog.hide(answer);
        } else {
            if ($scope.categoria.nombre == ""){
                $mdToast.show($mdToast.simple().content('Debe completar todos los campos!'));
            } else {
                var db = new PouchDB("351Bistro");
                db.get('categorias').then(function (doc) {
                    var array = doc.datos;
                    var id;
                    if(doc.datos.length != 0) {
                        var length = doc.datos.length - 1;
                        id = doc.datos[length].id + 1;
                    } else {
                        id = 1;
                    }
                    $scope.categoria.id = id;
                    array.push($scope.categoria);
                    doc.datos = array;
                    console.log(doc.datos);
                    console.log(id);
                    return db.put(doc);
                });
                $mdDialog.hide(answer);
            }
        }
    };
}

function SelectProductController($scope, $mdDialog, $mdToast) {
    $scope.producto  = {
        cantidad: 1,
        descuento: 0
    };
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        if (answer == "cancel"){
            $mdDialog.cancel();
        } else {
            $mdDialog.hide($scope.producto)
        }
    };
}

function AreaController($scope, $mdDialog, $mdToast) {
    $scope.area  = {
        nombre: '',
        id: ''
    };
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        if (answer == "cancel"){
            $mdDialog.hide(answer);
        } else {
            if ($scope.area.nombre == ""){
                $mdToast.show($mdToast.simple().content('Debe completar todos los campos!'));
            } else {
                var db = new PouchDB("351Bistro");
                db.get('areas').then(function (doc) {
                    // update his age
                    var array = doc.datos;
                    var id;
                    if(doc.datos.length != 0) {
                        var length = doc.datos.length - 1;
                        id = doc.datos[length].id + 1;
                    } else {
                        id = 1;
                    }
                    $scope.area.id = id;
                    array.push($scope.area);
                    doc.datos = array;
                    // put him back
                    console.log(doc.datos);
                    console.log(id);
                    return db.put(doc);
                });
                $mdDialog.hide(answer);
            }
        }
    };
}

function ProductoController($scope, $mdDialog, $mdToast) {

    var db = new PouchDB("351Bistro");
    $scope.colores = ["gris", "verde", "amarillo", "azul", "morado", "rojo"]
    db.get("categorias").then(function(doc){
        $scope.categorias = doc.datos;
    });
    $scope.producto  = {
        nombre: '',
        precio: '',
        id: ''
    };
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        if (answer == "cancel"){
            $mdDialog.hide(answer);
        } else {
            if ($scope.producto.nombre == "" || $scope.producto.precio == "" || $scope.producto.categoria == undefined){
                $mdToast.show($mdToast.simple().content('Debe completar todos los campos!'));
            } else {
                var db = new PouchDB("351Bistro");
                db.get('productos').then(function (doc) {
                    var array = doc.datos;
                    var id;
                    if(doc.datos.length != 0) {
                        var length = doc.datos.length - 1;
                        id = doc.datos[length].id + 1;
                    } else {
                        id = 1;
                    }
                    $scope.producto.id = id;
                    array.push($scope.producto);
                    doc.datos = array;
                    console.log(doc.datos);
                    console.log(id);
                    return db.put(doc);
                });
                $mdDialog.hide(answer);
            }
        }

    };
}