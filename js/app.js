angular.module( 'bistroGrill', [
    'ngAnimate',
    'ngMaterial',
    'ui.router',
    'ngStorage'
])

  .run(function($localStorage) {
      //var ngui = require('nw.gui');
      //var nwin = ngui.Window.get();
      //nwin.enterFullscreen();
      if (!$localStorage.logged){
          $localStorage.logged = false;
          $localStorage.loggedOut = true;
      }

      var pdb = new PouchDB('351Bistro');
      var mesas = {
          "_id": "mesas",
          "datos": [

          ]
      };
      var usuarios = {
          "_id": "usuarios",
          "datos": [
              {
                  username: "admin",
                  password: "1234",
                  nombre: "Administrator",
                  apellido:"",
                  cedula: "0",
                  tipo: "1",
                  id: "1"
              }
          ]
      };
      var categorias = {
          "_id": "categorias",
          "datos": [

          ]
      };
      var productos = {
          "_id": "productos",
          "datos": [

          ]
      };
      var areas = {
          "_id": "areas",
          "datos": [

          ]
      };
      var detalles = {
          "_id": "detalles",
          "datos": [

          ]
      };
      var facturas = {
          "_id": "facturas",
          "datos": [

          ]
      };
      pdb.put(areas);
      pdb.put(mesas);
      pdb.put(categorias);
      pdb.put(productos);
      pdb.put(usuarios);
      pdb.put(detalles);
      pdb.put(facturas);
  })

  .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode({
          enabled: true
      });
      // Ionic uses AngularUI Router which uses the concept of states
      // Learn more here: https://github.com/angular-ui/ui-router
      // Set up the various states which the app can be in.
      // Each state's controller can be found in controllers.js
      $stateProvider

          // setup an abstract state for the tabs directive
        .state('home', {
            url: "/",
            templateUrl: "templates/home.html"
        });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/');

  });